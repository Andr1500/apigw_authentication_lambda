# Simple serverless architecture using API Gateway, Lambda Authorizer and Secrets Manager

My post on Medium about the project: https://medium.com/p/b26a3c3c7302

## Pre-reqs

Before you start, make sure the following requirements are met:
- An AWS account with permissions to create resources.
- AWS CLI installed on your local machine.

## Building & Running 

1. Clone the repository and go to the repository folder.

2. Fill in all necessary Parameters in root.yaml , retrieve_invoke_url.sh and create CloudFormation stack with a CloudFormation template.

```
aws cloudformation create-stack \
    --stack-name apigw-lambda-sm \
    --template-body file://root.yaml \
    --capabilities CAPABILITY_NAMED_IAM \
    --parameters ParameterKey=AuthorizationTokenValue,ParameterValue="token_value" \
    --region eu-central-1 \
    --disable-rollback
```

3. Retrieve the Invoke Url of the Stage with retrieve_invoke_url.sh script:

```
./retrieve_invoke_url.sh
```

3. Make simple test of the API with curl command:

```
export APIGW_TOKEN='token_value'
curl -X GET -H "Authorization: $APIGW_TOKEN" invoke_url/invoke
```

4. Delete the CloudFormation stack

```
aws cloudformation delete-stack --stack-name apigw-lambda-sm
```

## Infrastructure schema and test result

![schema](images/apigw_lambda_sm.png)

```
./retrieve_invoke_url.sh 
Invoke URL: https://api_id.execute-api.eu-central-1.amazonaws.com/dev

export APIGW_TOKEN='token_value'
curl -X GET -H "Authorization: $APIGW_TOKEN" https://api_id.execute-api.eu-central-1.amazonaws.com/dev/invoke
"Simple Main lambda function responce"

 curl -X GET -H "Authorization: INCORRECT_TOKEN" https://api_id.execute-api.eu-central-1.amazonaws.com/dev/invoke
{"message":"Forbidden"}
```

You can support me with a virtual coffee https://www.buymeacoffee.com/andrworld1500 .